#include <gtest/gtest.h>
#include <vertex.hpp>

TEST(vertex, construct)
{
  model::vertex v{ 1 };
}

TEST(vertex, getters)
{
  model::vertex v{ 7, "ala ma kota" };
  EXPECT_EQ(7, v.index());
  EXPECT_EQ("ala ma kota", v.label());
}

TEST(vertex, adjacent_vertice)
{
  model::vertex v{ 7, "ala ma kota" };
  v.adjacent_vertices().push_back(5);
  v.adjacent_vertices().push_back(2);
  EXPECT_EQ(2, v.adjacent_vertices().size());
}

TEST(vertex, comparator)
{
  model::vertex va{ 1, "ala ma kota" };
  model::vertex vb{ 1 };
  model::vertex vc{ 3, "pebcac" };
  EXPECT_TRUE(model::vertex::less()(va, vc));
  EXPECT_TRUE(model::vertex::less()(vb, vc));
  EXPECT_FALSE(model::vertex::less()(vc, vc));
}