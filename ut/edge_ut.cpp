#include <gtest/gtest.h>
#include <edge.hpp>
#include <stdexcept>

TEST(edge, construct)
{
  model::edge{ 1, 2 };
  model::edge{ 1, 2, 7 };
}

TEST(edge, throw_on_vertex_loop)
{
  EXPECT_THROW(model::edge(7, 7), std::invalid_argument);
  EXPECT_NO_THROW(model::edge(7, 77));
}

TEST(edge, getters)
{
  model::edge e{ 1, 2, -3 };
  EXPECT_EQ(1, e.from());
  EXPECT_EQ(2, e.to());
  EXPECT_EQ(-3, e.weight());
}

TEST(edge, from_must_be_smaller_than_to)
{
  model::edge ea{ 77, 7 };
  model::edge eb{ 777, 77 };
  EXPECT_EQ(7, ea.from());
  EXPECT_EQ(77, ea.to());
  EXPECT_EQ(77, eb.from());
  EXPECT_EQ(777, eb.to());
}

TEST(edge, comparator)
{
  model::edge ea{ 7, 77};
  model::edge eb{ 7, 77 };
  model::edge ec{ 77, 777 };
  EXPECT_TRUE(model::edge::less()(ea, ec));
  EXPECT_TRUE(model::edge::less()(eb, ec));
  EXPECT_FALSE(model::edge::less()(ea, eb));
  EXPECT_FALSE(model::edge::less()(ec, ec));
}