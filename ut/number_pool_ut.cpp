#include <gtest/gtest.h>
#include <number_pool.hpp>

TEST(number_pool, construct)
{
  model::number_pool();
}

TEST(number_pool, fetch_without_returning)
{
  model::number_pool np;
  EXPECT_EQ(1, np.get());
  EXPECT_EQ(2, np.get());
  EXPECT_EQ(3, np.get());
  EXPECT_EQ(4, np.get());
  EXPECT_EQ(5, np.get());
  EXPECT_EQ(6, np.get());
}

TEST(number_pool, fetch_with_returning)
{
  model::number_pool np;
  EXPECT_EQ(1, np.get());
  EXPECT_EQ(2, np.get());
  EXPECT_EQ(3, np.get());
  np.put_back(2);
  EXPECT_EQ(2, np.get());
  EXPECT_EQ(4, np.get());
  np.put_back(1);
  np.put_back(3);
  np.put_back(4);
  EXPECT_EQ(1, np.get());
  EXPECT_EQ(3, np.get());
  EXPECT_EQ(4, np.get());
  EXPECT_EQ(5, np.get());
}