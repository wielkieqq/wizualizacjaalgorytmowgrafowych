#include <gtest/gtest.h>
#include <graph.hpp>
#include <stdexcept>

TEST(graph, construct)
{
  model::graph();
}

TEST(graph, adding_vertices)
{
  model::graph g;
  g.add_vertex("ala");
  g.add_vertex("ma");
  g.add_vertex("kota");
  EXPECT_EQ(3, g.vertices().size());
  for (const auto& v : g.vertices())
    EXPECT_TRUE(v.second.adjacent_vertices().empty());
}

TEST(graph, removing_vertices_reuses_id)
{
  model::graph g;
  g.add_vertex("ala");
  g.add_vertex("ma");
  g.add_vertex("kota");
  g.remove_vertex(2);
  EXPECT_EQ(2, g.vertices().size());
  g.add_vertex("foo");
  EXPECT_EQ(3, g.vertices().size());
  EXPECT_EQ("foo", g.vertices().at(2).label());
  EXPECT_EQ(2, g.vertices().at(2).index());
  g.add_vertex("foo222");
  EXPECT_EQ("foo222", g.vertices().at(4).label());
  EXPECT_EQ(4, g.vertices().at(4).index());
  EXPECT_THROW(g.remove_vertex(5), std::invalid_argument);
}

TEST(graph, adding_and_removing_edges)
{
  model::graph g;
  g.add_vertex("ala"); //1
  g.add_vertex("ma"); //2
  g.add_vertex("kota"); //3
  g.add_edge(1, 2);
  ASSERT_EQ(1, g.vertices().at(1).adjacent_vertices().size());
  EXPECT_EQ(2, g.vertices().at(1).adjacent_vertices().front());
  ASSERT_EQ(1, g.vertices().at(2).adjacent_vertices().size());
  EXPECT_EQ(1, g.vertices().at(2).adjacent_vertices().front());
  ASSERT_EQ(1, g.edges().size());
  EXPECT_NE(g.edges().end(), g.edges().find({ 1, 2 }));
  g.add_edge(3, 2);
  ASSERT_EQ(1, g.vertices().at(3).adjacent_vertices().size());
  EXPECT_EQ(2, g.vertices().at(3).adjacent_vertices().front());
  ASSERT_EQ(2, g.vertices().at(2).adjacent_vertices().size());
  EXPECT_EQ(1, g.vertices().at(2).adjacent_vertices().front());
  EXPECT_EQ(3, g.vertices().at(2).adjacent_vertices().back());
  ASSERT_EQ(2, g.edges().size());
  EXPECT_NE(g.edges().end(), g.edges().find({ 1, 2 }));
  EXPECT_NE(g.edges().end(), g.edges().find({ 3, 2 }));
  g.remove_edge(2, 3);
  ASSERT_EQ(1, g.vertices().at(1).adjacent_vertices().size());
  EXPECT_EQ(2, g.vertices().at(1).adjacent_vertices().front());
  ASSERT_EQ(1, g.vertices().at(2).adjacent_vertices().size());
  EXPECT_EQ(1, g.vertices().at(2).adjacent_vertices().front());
  EXPECT_EQ(0, g.vertices().at(3).adjacent_vertices().size());
  ASSERT_EQ(1, g.edges().size());
  EXPECT_NE(g.edges().end(), g.edges().find({ 1, 2 }));
  g.remove_edge(2, 1);
  EXPECT_TRUE(g.edges().empty());
  EXPECT_TRUE(g.vertices().at(1).adjacent_vertices().empty());
  EXPECT_TRUE(g.vertices().at(2).adjacent_vertices().empty());
  EXPECT_TRUE(g.vertices().at(3).adjacent_vertices().empty());
}

TEST(graph, removing_vertex_deletes_edges)
{
  model::graph g;
  g.add_vertex("ala"); //1
  g.add_vertex("ma"); //2
  g.add_vertex("kota"); //3
  g.add_edge(1, 2);
  g.add_edge(3, 2);
  g.add_edge(1, 3);
  g.remove_vertex(2);
  EXPECT_EQ(1, g.edges().size());
  EXPECT_EQ(2, g.vertices().size());
  ASSERT_EQ(1, g.vertices().at(1).adjacent_vertices().size());
  EXPECT_EQ(3, g.vertices().at(1).adjacent_vertices().front());
  ASSERT_EQ(1, g.vertices().at(3).adjacent_vertices().size());
  EXPECT_EQ(1, g.vertices().at(3).adjacent_vertices().front());
  g.remove_vertex(3);
  EXPECT_EQ(0, g.edges().size());
  EXPECT_EQ(1, g.vertices().size());
  ASSERT_EQ(0, g.vertices().at(1).adjacent_vertices().size());
}
