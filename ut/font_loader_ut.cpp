#include <gtest/gtest.h>
#include <font_loader.hpp>

TEST(font_loader, constructor)
{
  EXPECT_NO_THROW(font_loader("ut_rsrc/font.fnt"));
}

TEST(font_loader, throw_on_nonexisting_fontfile)
{
  EXPECT_ANY_THROW(font_loader("asdasd.fnt"));
}

TEST(font_loader, font_name)
{
  font_loader my_font("ut_rsrc/font.fnt");
  EXPECT_STRCASEEQ("Comic Sans MS", my_font.get_name().c_str());
}

TEST(font_loader, page_names)
{
  font_loader my_font("ut_rsrc/font.fnt");
  ASSERT_EQ(1, my_font.get_page_filenames().size());
  EXPECT_STRCASEEQ("font_0.tga", my_font.get_page_filenames().front().c_str());
}

TEST(font_loader, character_details)
{
  font_loader my_font("ut_rsrc/font.fnt");
  ASSERT_EQ(285, my_font.get_char_details().size());
  //space
  //char id=32   x=165   y=64    width=3     height=1     xoffset=-1    yoffset=30    xadvance=7     page=0  chnl=15
  const auto& first_char = my_font.get_char_details().front();
  EXPECT_EQ(32, first_char.id);
  EXPECT_EQ(165, first_char.x);
  EXPECT_EQ(64, first_char.y);
  EXPECT_EQ(3, first_char.width);
  EXPECT_EQ(1, first_char.height);
  EXPECT_EQ(-1, first_char.xoffset);
  EXPECT_EQ(30, first_char.yoffset);
  EXPECT_EQ(7, first_char.xadvance);
  EXPECT_EQ(0, first_char.page);
  EXPECT_EQ(15, first_char.chnl);

  //weird-ass symbol
  //char id=1169 x=402   y=100   width=10    height=15    xoffset=1     yoffset=9     xadvance=11    page=0  chnl=15
  const auto& last_char = my_font.get_char_details().back();
  EXPECT_EQ(1169, last_char.id);
  EXPECT_EQ(402, last_char.x);
  EXPECT_EQ(100, last_char.y);
  EXPECT_EQ(10, last_char.width);
  EXPECT_EQ(15, last_char.height);
  EXPECT_EQ(1, last_char.xoffset);
  EXPECT_EQ(9, last_char.yoffset);
  EXPECT_EQ(11, last_char.xadvance);
  EXPECT_EQ(0, last_char.page);
  EXPECT_EQ(15, last_char.chnl);

  //m like miodozer
  //slot 77 in the file
  //char id=109  x=62    y=125   width=17    height=12    xoffset=1     yoffset=12    xadvance=18    page=0  chnl=15
  const auto& letter_m = my_font.get_char_details()[77];
  EXPECT_EQ(109, letter_m.id);
  EXPECT_EQ(62, letter_m.x);
  EXPECT_EQ(125, letter_m.y);
  EXPECT_EQ(17, letter_m.width);
  EXPECT_EQ(12, letter_m.height);
  EXPECT_EQ(1, letter_m.xoffset);
  EXPECT_EQ(12, letter_m.yoffset);
  EXPECT_EQ(18, letter_m.xadvance);
  EXPECT_EQ(0, letter_m.page);
  EXPECT_EQ(15, letter_m.chnl);
}