\documentclass[declaration, shortabstract, inz, polish]{iithesis}

\usepackage[utf8]{inputenc}
\usepackage{outlines}
\usepackage{graphicx}
\usepackage{float}

\polishtitle    {Visualisation of algorithms on graphs}
\englishtitle   {Wizualizacja algorytmów grafowych}

\polishabstract {Niniejsza praca jest implementacją aplikacji służącej do wizualizacji algorytmów grafowych w czasie rzeczywistym. Sam program to framework napisany w c++11 z użyciem opengl, przenośny między systemami operacyjnymi. Na kluczowe komponenty składają się graficzny system okienkowy napisany od podstaw, renderowanie sceny oraz czcionek bezpośrednio na GPU oraz system wydarzeń i wiadomości otwierający możliwość rozdzielania elementów systemu poprzez informowanie wyspecjalizowanych komponentów o konkretnych zdarzeniach. Główną ideą kodu jest łatwość w dodawaniu nowych algorytmów oraz łatwość w użyciu programu. Decyzje projektowe podjęte w fazie projektowania udogadniają rozbudowę, istotnie, dodanie nowej wizualizacji czy kontrolki interfejsu użytkownika sprowadza się do specializacji pewnych już istniejących klas bazowych.}

\englishabstract{The thesis is an implementation of realtime desktop application used to visualise algorithms on graph-like structures. Program itself is a framework written in C++11 with opengl, portable through most OS. Key components include graphical user interface system written from scratch, rendering the scene and font directly on the GPU and an event system which opens possibilities to decouple components of the system by dispatching messages and all kinds of events to specialised components. Primary idea behind the code is ease in use and ease in adding new algorithms. Early implementation and design decissions facilitate extensibility, and in fact, adding new algorithm visualisation or graphical interface widget comes to extending existing base classes.}

\author         {Krzysztof Pyrkosz}

\advisor        {dr Andrzej Łukaszewski}
%\date          {}                     % Data zlozenia pracy
% Dane do oswiadczenia o autorskim wykonaniu
%\transcriptnum {}                     % Numer indeksu
%\advisorgen    {dr. Jana Kowalskiego} % Nazwisko promotora w dopelniaczu
%%%%%


\begin{document}

\chapter{Wprowadzenie}

\section{Koncepcja}
Od czasów licealnych interesowały mnie zagadnienia związane z renderowaniem w czasie rzeczywistym, w jaki sposób maszyna jest w stanie wyrysować ogromną ilość modeli sprawiając wrażenie niesamowitej płynności. Podczas studiów najwięcej frajdy sprawiały mi kursy programowania niskopoziomowego, w szczególności \textit{Podstawy grafiki komputerowej} gdzie możliwości optymalizacji oraz manipulacji obrazem były dosłownie nieograniczone.

Dlaczego wizualizacja algorytmów grafowych? Podczas intensywnych przygotowań do egzaminu z \textit{Algorytmów i struktur danych} natrafiłem na stronę internetową \cite{vis} na której to dostępne są wizualizacje wielu klasycznych kolekcji, od podstaw takich jak kolejki, przez kopce, tablice haszujące aż po liczne struktury drzewiaste. Po zdanym egzaminie narodził się pomysł napisania czegoś podobnego, lecz w nieco innym formacie.

\section{Realizacja}
Od pierwszej linii kodu projekt jest placem zabaw. Myślą przewodnią było stworzenie komponentów nad którymi nigdy wcześniej nie miałem okazji pracować, takich jak system wydarzeń, interfejs użytkownika z użyciem jedynie niskopoziomowego API. Każdą kluczową część starannie projektowałem a następnie przemieniałem w kod źródłowy. Oczywiście nie obyło się bez rozczarowań oraz problemów które wyszły w procesie implementacji. Te opiszę szczegółowo w kolejnych rozdziałach.

\chapter{Opis głównych komponentów}

\section{Podsystem wiadomości}

Sercem systemu jest podsystem odpowiedzialny za kolejkowanie wiadomości i odpowiednie notyfikowanie subskrybentów zainteresowanych zdarzeniami. Najistotniejszymi elementami tego podsystemu są:

\begin{outline}
	\1 generyczny interfejs \textit{event} będący bazą dla zdarzeń z jednej kategorii,
	\1 \textit{event\_sink} do którego spływają konkretne zdarzenia,
	\1 \textit{event\_handler} z metodą wirtualną \textit{on\_event()} służącą do powiadomienia o zajściu.
\end{outline}

Implementacja nie wymusza jednej wielkiej kolejki, obsługującej każdy typ zdarzenia, dla całej aplikacji. Poprzez generyczność klasy bazowej \textit{event} mogę tworzyć hierarchie nieskorelowanych ze sobą klas reprezentujących przeróżne zdarzenia --- zarówno kliknięcie guzika jak i zakończenie algorytmu wysyła wiadomość, lecz obie klasy nie dzielą wspólnej bazy. Zapewnia mi to wiele korzyści, takich jak zdecentralizowanie i łatwość w utworzeniu zupełnie nowej kategorii wydarzeń. Nie istnieje możliwość omyłkowego dodania niekompatybilnej wiadomości do innej kolejki niż ta, którą ma się za typ templatowy klasy bazowej, poprawność pilnowana jest już w trakcie kompilacji.

Implementacja opiera się o \textit{double dispatch}, czyli dwa wywołania wirtualne --- jedno w klasie \textit{event} której konkretny pochodny typ zapominany jest w trakcie kolejkowania, drugie --- w momencie w którym klasa daje o tym znać z powrotem o swoim konkretnym typie. Całość przypomina wzorzec \textit{event dispatcher}, a sposób działania metaprogramowania w C++ ułatwia zadanie i pozwala wygenerować sporych rozmiarów klasę bez duplikacji w kodzie źródłowym.

Komponenty generujące zdarzenia oraz te zainteresowane są odpowiednio rozdzielone. Generujące nie muszą nawet znać konkretnego dispatchera, wystarczy im sam \textit{event\_sink}. Zainteresowane implementują interfejs \textit{event\_handler}, który dyskretnie w konstruktorze wymusza rejestrację, a w destruktorze wypisuje element z kolekcji zainteresowanych.

Dobrym przykładem na zobrazowanie działania tego podsystemu jest klasa \textit{log\_window}. Nie ma w niej jakiejkolwiek informacji o reszcie systemu, nawet nie wie nic o klasach które generują tego typu wiadomości, jedyne czym jest zainteresowana to otrzymywanie powiadomień typu \textit{log\_msg} i dodawanie ich do okienka z logiem działania programu. Nic nie stoi na przeszkodzie aby zaimplementować kolejny \textit{event\_handler} dla \textit{log\_msg} i wypisywać wiadomości na standardowym wyjściu lub do pliku.

W programie wyróżniłem trzy kategorie wydarzeń:

\begin{outline}
	\1 dla wiadomości z okna (klik, ruch myszką, naciśnięcie klawisza klawiatury)
	\1 dla interfejsu użytkownika (naciśnięty guzik interfejsu)
	\1 dla głównej aplikacji (algorytm skończył działanie i dodanie linii do logu tekstowego)
\end{outline}

\newpage

\begin{figure}[H]
	\centering
	\includegraphics{event.png}
	\caption{Klasa bazowa event oraz kolejka wydarzeń GUI}
\end{figure}

\section{Podsystem interfejsu użytkownika}

Jako że jednym z głównych założeń było działanie w czasie rzeczywistym i interaktywna współpraca z użytkownikiem, zaistniała potrzeba stworzenia warstwy pośredniczącej między oknem a modelem danych.

\subsection{GUI}
Klasa \textit{gui} reprezentuje graficzny interfejs użytkownika jako kolekcję abstrakcyjnych \textit{widget}'ów. Odpowiada za:

\begin{outline}
	\1 zainicjowanie wszystkich składowych interfejsu
	\1 odpowiednią obsługę kliknięć i naciśnięć klawiszy klawiatury
	\1 dynamiczne tworzenie i usuwanie okienek popup
\end{outline}

\begin{figure}[H]
	\centering
	\includegraphics{gui2.png}
	\caption{Guziki play, pause, step, stop, left oraz pole tekstowe}
\end{figure}

\subsection{Widget}
Klasa \textit{widget} to interfejs będący bazą wszystkich pochodnych kontrolek. Konkretne implementacje muszą doprecyzować pewne metody, między innymi obsługujące klik i rysujące to okno.

\subsection{Button}
Klasa \textit{button} reprezentuje klikalny guzik. Domyślnie, kliknięcie powoduje zgłoszenie wiadomości \textit{button\_pressed} z argumentem będącym nazwą guzika. Takie zachowanie wystarcza w przypadku guzików \textit{step} albo \textit{pause}, lecz nie dla \textit{left} czy \textit{right}, których jedynym zadaniem jest zmiana tekstu na polu tekstowym z wyborem algorytmu. Guzik play dla odmiany musi sczytać to pole i dodatkowo przekazać jego treść razem z wiadomością o kliknięciu. Dlatego istnieje drugi konstruktor, który przyjmuje lambdę którą wywołuje podczas kliku.

\subsection{Textfield}
Klasa \textit{textfield} jest prostą implementacją której zadaniem jest przechowywanie tekstu. Obsługa wejścia z klawiatury jest opcjonalna, można utworzyć pole tekstowe statyczne (po polu wyboru algorytmu pisać się nie da ) lub dynamiczne (eksport lub import oczekuje wpisania nazwy pliku).

\subsection{Log window}
Klasa \textit{log\_window} służy do wyświetlania logu działania programu, a konkretniej wszystkich wiadomości typu \textit{log\_msg}. Wyświetlany jest po lewej stronie okna.

\subsection{Popup}
Dekorator \textit{floating\_element} użyty jest do tworzenia tymczasowych okien. Jego głównym celem jest owrapowanie dowolnego innego \textit{widget}'a i usunięcie go w momencie kliknięcia poza polem wrapowanego elementu. Dla przykłądu kontrolka \textit{query\_box} pojawia się dynamicznie gdy klikniemy guzik \textit{export}.

\begin{figure}[H]
	\centering
	\includegraphics{query_box.png}
	\caption{Klik poza obszar \textit{query\_box}'a powoduje jego usunięcie}
\end{figure}

\subsection{Gui\_brush}
Klasa \textit{gui\_brush} udostępnia elementom interfejsu użytkownika możliwość wyrysowania siebie. Posiada metody rysowania tekstu oraz tekstury. Dowolna implementacja \textit{widget}'a która może pojawić się w przyszłości ma w ten sposób zapewnioną możliwość wyświetlenia się na pierwszym planie ekranu.

\section{Model danych}

\subsection{Vertex}
\textit{Vertex} reprezentuje wierzchołek grafu. Posiada atrybuty takie jak indeks, kolor i pozycja w świecie. Algorytm operujący na grafie może dowolnie przekolorować wierzchołek na potrzeby wizualizacji.

\subsection{Edge}
\textit{Edge}, jak sama nazwa wskazuje, to krawędź grafu. Zawiera referencje do obu wierzchołków które łączy, kolor oraz wagę. Podobnie jak w przypadku klasy \textit{vertex} może zostać przekolorowana podczas działania programu, a jej waga stanowi ważną informację dla algorytmu.

\subsection{Graph}
\textit{Graph} spina model w spójną całość. Zawiera kolekcję wierzchołków, kolekcję krawędzi oraz zapewnia do nich dostęp w kontrolowany sposób. Implementacja strategii algorytmu nie ma dostępu pozwalającego na usunięcie jakiejkolwiek składowej grafu, za to aplikacja ma już taką możliwość, na przykład na potrzeby importowania grafu do systemu.

\section{Renderowanie}

\subsection{Schemat ogólny}
Operacja rysowania świata odbywa się w kilku fazach. Nie istnieje potrzeba używania bufora głębokości ponieważ całość można namalować algorytmem malarza, zamazując elementy w tle tymi na bliższym planie. W pierwszej kolejności rysowane są krawędzie jako proste odcinki między wierzchołkami. Następnie same wierzchołki, zakrywając końcówki krawędzi i w kolejnym kroku pojawiają się liczby oznaczające wagi. Po obsłużeniu grafu następuje rysowanie interfejsu użytkownika. Klasa \textit{gui} wywołuje kolejno metodę wirtualną \textit{draw} na każdym elemencie swojej listy.

\subsection{Renderowanie tekstu}
Sama karta graficzna nie zna koncepcji litery - dla niej istnieją bardziej prymitywne twory - kolekcje trójkątów, tekstury, wektory czy macierze. Dlatego rysowanie tekstu jest ciekawym zagadnieniem, gdyż z podstawowych cegiełek należy zbudować program wyświetlający dowolny tekst.

Jednym z podejść jest wygenerowanie atlasu liter wraz z definicją glifów (np. za pomocą programu BMFont \cite{BMFont}). Wyspecjalizowany \textit{shader} (program karty graficznej) przyjmuje teksturę atlasu, lokalizację prostokątnego glifu dla zadanej litery oraz macierz reprezentującą pozycję i skalę na ekranie. Włączony \textit{alpha blending} powoduje, że piksele o współczynniku przezroczystości 1 (sama litera) pojawiają się na ekranie, a przezroczyste (o współczynniku 0, tło litery) nie zamalowują powierzchni.

\begin{figure}[H]
	\centering
	\includegraphics{arialatlas.png}
	\caption{Przykładowy atlas czcionki Arial, rozmiar 15}
\end{figure}

\chapter{Jak korzystać z programu?}

\section{Guziki GUI}

\begin{figure}[H]
	\centering
	\includegraphics{gui.png}
\end{figure}

Od lewej do prawej:

\begin{outline}
	\1 play - włącza wybrany algorytm, lub gdy algorytm działa w trybie krokowym włącza tryb automatyczny
	\1 pause - pauzuje automatyczne wykonanie przechodząc do trybu krokowego
	\1 step - w trybie krokowym wykonuje jeden krok algorytmu
	\1 stop - przerywa działanie algorytmu
	\1 left/right - zmieniają wybór algorytmu w polu tekstowym pomiędzy nimi
	\1 refresh - losuje graf przypominający siatkę
	\1 export - eksportuje widoczny na ekranie graf do pliku
	\1 import - importuje graf z pliku do aplikacji
\end{outline}

\section{Uruchamianie i przerywanie algorytmu}
Wybrany algorytm startujemy naciskając guzik play. Zaczynamy w trybie krokowym. Ponowne naciśnięcie play spowoduje wykonywanie 3 kroków na sekundę. Program jest odporny na podstawowe błędy takie jak importowanie grafu w trakcie działania algorytmu. Aby wczytać lub wylosować nowy graf, algorytm musi się skończyć bądź zostać zatrzymany guzikiem stop. W obecnym stanie zaimplementowane są algorytmy Dijkstry, Kruskala, BFS oraz DFS.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{exampleusage.png}
	\caption{Działanie algorytmu Dijkstry}
\end{figure}

\section{Przydatne skróty klawiszowe}
\begin{outline}
	\1 DELETE - usuwa zaznaczone wierzchołki u krawędzie, tylko w trybie edycji
	\1 A - odznacza wszystkie zaznaczone krawędzie i wierzchołki - na przykład gdy chcemy odznaczyć wszystko aby zaznaczyć startowy wierzchołek dla algorytmu
	\1 V - przechodzi do trybu manualnego dodawania wierzchołków (lewy guzik myszy dodaje wierzchołek w punkcie kliknięcia). Ten sam guzik wraca do trybu domyślnego
	\1 E - w trybie edycji łączy wszystkie zaznaczone wierzchołki krawędzią o wadze 0.
	\1 Strzałki klawiatury przesuwają graf na ekranie
	\1 Rolka myszy oddala oraz przybliża widok grafu
\end{outline}

\section{Dodawanie własnego algorytmu}
To jest bardzo proste! Dziedziczymy z klasy \textit{graph\_algorithm}, implementujemy konstruktor w którym dokonujemy wszelkiej maści przetwarzania wstępnego oraz metodę \textit{step} wykonującą jeden krok algorytmu. Dla przykładu, algorytm Kruskala wstawia krawędzie do kolejki priorytetowej według wag, a algorytm Dijkstry buduje listy incydencji dla wierzchołków korzystając z funkcji z przestrzeni nazw \textit{graph\_helpers}.

W ostatnim rozdziale znajduje się szczegółowy opis implementacji takiego algorytmu na przykładzie sprawdzania dwudzielności.

\section{Dodawanie własnej kontrolki GUI}
Aby dodać nową kontrolkę, na przykład okienko wyświetlające stan kolejki w algorytmie BFS wystarczy zaimplementować metody wirtualne klasy \textit{widget} i przekazać referencję do kolejki w konstruktorze. Implementacja rysowania może być całkowicie dowolna.

\chapter{Budowanie aplikacji i zależności}

\section{Kompilacja}
Program od początku pisany był z naciskiem na przenośność. Nie posiada zależności wyłącznych dla konkretnego systemu operacyjnego. Pliki projektowe generowane są za pomocą CMake \cite{CMake}. Wymagany jest kompilator wspierający c++11. Budowa możliwa jest w trybach \textit{Debug} oraz \textit{Release}. Skrypty budujące automatycznie kopiują katalog \textit{rsrc} (resources) do katalogu w którym pojawia się wynikowy plik wykonywalny, dzięki czemu skompilowany program można uruchomić bez potrzeby ręcznego manipulowania zasobami.

\section{Struktura katalogów}
\begin{outline}
	\1 inc/ - pliki nagłówkowe .hpp
	\1 src/ - pliki źródłowe .cpp
	\1 rsrc/ - zasoby, takie jak tekstury, przykładowe grafy oraz shadery
	\1 soil/ - biblioteka o otwartym źródle pomocna przy ładowaniu tekstur
\end{outline}

\section{Zależności}
Projekt korzysta z kilku popularnych bibliotek ułatwiających pracę z opengl. Wszystkie niżej wymienione można z łatwością zainstalować Linuxowym \textit{apt}'em, a pod Windowsem \textit{vcpkg}. CMake znajduje wszystkie zależności samemu, a gdy brakuje lub nie jest w stanie którejś znaleźć, wyświetla stosowny komunikat.

Wymagane jest uprzednie zainstalowanie:

\begin{outline}
	\1 glm \cite{glm}
	\1 glfw \cite{glfw}
	\1 glew \cite{glew}
\end{outline}

\chapter{Wnioski z pracy}

\section{Czego nauczyłem się pisząc pracę}
Udało mi się zrealizować projekt na czas stosując w praktyce metodologię \textit{Kanban} poznaną na zajęciach z \textit{Inżynierii oprogramowania}. Cały projekt pisałem od zera, nie obyło się bez zaskoczeń oraz nieoczekiwanych błędów, tablica z pełnym statusem postępów bardzo ułatwiła zadanie.

Z części programistycznej, w systemie eventów wymienić mogę maksymalne przerzucenie odpowiedzialności z człowieka na kompilator. Początkowo nie używałem \textit{template}'ów, co dosyć szybko skończyło się widoczną duplikacją kodu. Po zmianie na obecne, generyczne podejście ilość kodu w podsystemie jest mała i w głównej mierze niezmienna, a wynikowe kolejki mogą obsługiwać dowolną ilość konkretnych typów zdarzeń.

We wczesnej fazie implementacji interfejsu użytkownika pod Linuxem nie działało renderowanie czcionek, pomimo braku ostrzeżeń podczas kompilacji. Miałem wtedy okazję poznać oraz skorzystać po raz pierwszy z sanitizera który wykrył źródło problemu. Dokonałem rzutowania wskaźnika zakładając, że typ \textit{wchar\_t} jest zawsze dwubajtowy, na Linuxie okazuje się, ze ma 4 bajty. Innym problemem wykrytym przez sanitizer był \textit{undefined behavior} w klasie \textit{binary\_deserializer}. Parsując wejściowy plik w formacie binarnym dokonywałem dereferencji niezalignowanego wskaźnika. Co ciekawe, program działał i wczytywał dane poprawnie, lecz standard mówi, że w ogólności nie musi tak być.

Ostatnim i zarazem bardzo ważnym doświadczeniem było poprawne wydzielenie czystych interfejsów. Zaowocowało to przede wszystkim w końcowej fazie projektu, gdzie zaimplementowanie konkretnych algorytmów odbyło się jedynie przez dziedziczenie jednej klasy i zaimplementowanie jednej metody. \textit{Loose coupling} powoduje, że algorytm operujący na grafie może zostać uruchomiony nawet bez okna aplikacji. Część kontrolera oraz modelu nie niesie jakiejkolwiek informacji o stronie graficznej. Projekt przypomina wzorzec model-view-controller, gdzie modelem danych jest graf, widokiem okno programu a kontrolerem GUI.

\section{Dylematy implementacyjne}
Pomimo pełnej sprawności programu implementacja niektórych komponentów mogłaby być nieco zmieniona. W pierwszej kolejności rozdzieliłbym graf na dwie klasy, jedna jako model, druga atrybuty takie jak kolor czy lokalizacja wierzchołków.

W przypadku renderowania tekstu, bardziej skalowalną metodą byłoby rysowanie całych linii na raz, zamiast litera po literze. Są to jednak modyfikacje ''coś za coś'', których pozytywne strony ujawniłyby się przy rozmiarach danych których nie planuję dla swojego programu. Przedwczesna optymalizacja detali nie kończy się zwykle najlepiej.

Udostępniłbym również graf algorytmom jako nie-\textit{const}, tak aby mogły usuwać lub dodawać krawędzie. Wstępnie założyłem, że algorytm będzie jedynie czytał i przekolorowywał elementy, co gwarantuje \textit{const}'owość.

\section{Zakończenie}
W obecnej formie projekt jest gotowy na rozszerzanie, a przykładowe algorytmy działają całkowicie poprawnie. Z czystej ciekawości uruchomiłem algorytm Kruskala na grafie ''siatce'' z dodatkowymi ukośnymi krawędziami, gdzie wszystkie wagi są losowe. Efekt końcowy wyszedł dosyć ciekawy - faktycznie nietrudno jest udowodnić, że algorytm Kruskala, tak jak i inne algorytmy generujące minimalne drzewo rozpinające nadają się do generowania labiryntów, niezależnie od tego, czy za ścieżki labiryntu uznamy wyróżnione krawędzie, czy przestrzenie między nimi. Oto rezultat:

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{kruskal.png}
	\caption{Labirynt wygenerowany algorytmem Kruskala}
\end{figure}

\chapter{Appendix - dodawanie algorytmu na przykładzie}

Aby dodać nową wizualizację, należy:

\begin{outline}
	\1 zaimplementować klasę reprezentującą algorytm, pochodną \textit{graph\_algorithm}
	\1 dodać tekst do kolekcji nazw algorytmów w \textit{gui.cpp}, aby można było dokonać wyboru tego algorytmu używając strzałek lewo-prawo w górnym menu
	\1 dodać obsługę wyboru nowego algorytmu \textit{on\_event(const e\_play\_request)}
\end{outline}

\section{Implementacja algorytmu sprawdzającego dwudzielność grafu}
W pierwszej kolejności tworzymy plik źródłowy \textit{biparite.cpp} w katalogu \textit{src} oraz nagłówkowy \textit{biparite.hpp} w \textit{inc}. Dodajemy oba pliki do zestawu \textit{SOURCES} oraz \textit{INCLUDES} w CMakeLists.txt, aby zostały dołączone do projektu.

Zabieramy się za implementację klasy. Dołączamy plik nagłówkowy klasy bazowej \textit{graph\_algorithm.hpp}. Dodajemy również \textit{game\_event\_dispatcher.hpp} celem generowania wydarzeń \textit{log\_msg} oraz zakończenia algorytmu. Potrzebować będziemy jeszcze \textit{graph\_helpers.hpp} aby wygenerować listy incydencji w konstruktorze (sam model grafu pamięta tylko zestaw wierzchołków oraz krawędzi, bez nadmiarowych informacji).

Dziedziczymy po \textit{graph\_algorithm}, deklarujemy składowe klasy potrzebne do działania - zestaw wierzchołków do odwiedzenia oraz listy incydencji. Zapisujemy również referencję do \textit{game\_event\_sink} aby podczas wykonywania kroków wizualizacji wysyłać wiadomości do reszty systemu. Sercem klasy jest metoda wirtualna \textit{void biparite::step()}, odpowiedzialna za przeprowadzenie jednego kroku działania konkretnego algorytmu.

W naszym przykładzie sprawdzania dwudzielności implementacja wygląda tak. Jeśli obsłużyliśmy wszystkie wierzchołki, oznacza to, że algorytm przekolorował graf bez błędów, czyli graf jest dwudzielny. Wysyłamy wiadomość o zakończeniu algorytmu oraz wiadomość tekstową do logu. Następnie sprawdzamy czy kolekcja sąsiadów do odwiedzenia jest pusta. Może się tak zdarzyć, gdy graf nie jest spójny. W tym przypadku po prostu rozpatrujemy kolejny podgraf zaczynając z dowolnego wierzchołka. W końcu pobieramy kolejny wierzchołek do odwiedzenia i sprawdzamy sąsiadów. Jeśli nie został jeszcze przekolorowany (czyli jest w kolorze \textit{color::white}) kolorujemy go kolorem przeciwnym niż kolor obecnie rozpatrywanego wierzchołka. Jeśli okaże się, że kolor któregoś sąsiada jest taki jak nasz, przerywamy algorytm i wysyłamy stosowną wiadomość do logu dla użytkownika. Po udanym przekolorowaniu logujemy informację o zakończeniu kroku.

\section{Dodanie możliwości wyboru nowego algorytmu}
Klasa reprezentująca nowy algorytm już istnieje, lecz nie mamy możliwości uruchomienia go, ponieważ nie da się go wybrać z przewijanej listy interfejsu użytkownika. Jest ona inicjowana w \textit{gui.cpp}, a ponad nią znajduje się wektor \textit{std::wstring} zawierający nazwy algorytmów. Dopisujemy do niego nazwę która będzie reprezentować nowo dodany algorytm, np. ''Biparite''.

Od tego momentu naciśnięcie guzika \textit{play} podczas gdy wybrany jest nowy algorytm generuje prośbę uruchomienia algorytmu, lecz aplikacja sterująca wykonaniem (WiAGra.cpp) musi umieć przetłumaczyć tekst ''Biparite'' na konkretną podklasę \textit{graph\_algorithm}. Jeśli nasza klasa potrzebuje dodatkowych danych dla konstruktora (na przykład dokładnie jeden wierzchołek startowy dla algorytmu Dijkstry) w tym miejscu możemy zgłosić niespełnienie wymogów (gdy zaznaczymy ich zero, dwa lub więcej).

\begin{figure}
	\centering
	\includegraphics[scale=0.4]{biparite_pre.png}
	\caption{Przykładowy niespójny graf dwudzielny}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=0.4]{biparite_working.png}
	\caption{Algorytm w trakcie działania}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=0.4]{biparite_success.png}
	\caption{Pomyślne ukończenie działania}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=0.4]{biparite_fail.png}
	\caption{Wykrycie cyklu nieparzystego (dodałem jedną krawędź)}
\end{figure}

\begin{thebibliography}{1}
	
	\bibitem{vis} Data Structure Visualizations
	\\\texttt{https://www.cs.usfca.edu/\~{}galles/visualization/Algorithms.html}
	
	\bibitem{BMFont} BMFont
	\\\texttt{http://www.angelcode.com/products/bmfont/}
	
	\bibitem{CMake} CMake
	\\\texttt{https://cmake.org/}
	
	\bibitem{glm} OpenGL Mathematics
	\\\texttt{https://glm.g-truc.net/}
	
	\bibitem{glfw} Glfw
	\\\texttt{https://www.glfw.org/}
	
	\bibitem{glew} The OpenGL Extension Wrangler Library
	\\\texttt{http://glew.sourceforge.net/}		
		
\end{thebibliography}

\end{document}
